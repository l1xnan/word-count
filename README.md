# word-count

演示分别使用纯Python、单线程 Rust、多线程 Rust 实现文本搜索功能。
## 构建

```shell
pip install .
```

## 用法

```python
from word_count import search_py, search, search_sequential

search_py("foo bar", "foo")
search("foo bar", "foo")
search_sequential("foo bar", "foo")
```

## 测试

运行 nox:

```shell
nox
```

## 基准测试

运行 nox 进行基准测试:

```shell
nox -s bench
```
结果:
```
nox > Running session bench
nox > Creating virtual environment (virtualenv) using python.exe in .nox\bench
nox > python -m pip install -rrequirements-dev.txt
nox > python -m pip install .
nox > pytest --benchmark-enable
===================================================================================== test session starts ======================================================================================
platform win32 -- Python 3.10.5, pytest-7.1.2, pluggy-1.0.0
benchmark: 3.4.1 (defaults: timer=time.perf_counter disable_gc=False min_rounds=5 min_time=0.000005 max_time=1.0 calibration_precision=10 warmup=False warmup_iterations=100000)
rootdir: D:\Code\python-performance\word-count, configfile: pyproject.toml
plugins: benchmark-3.4.1
collected 4 items

tests\test_word_count.py ....                                                                                                                                                             [100%]


----------------------------------------------------------------------------------------------------------- benchmark: 4 tests ----------------------------------------------------------------------------------------------------------
Name (time in us)                                             Min                   Max                  Mean              StdDev                Median                 IQR            Outliers         OPS            Rounds  Iterations
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
test_word_count_rust_parallel                            188.6000 (1.0)        559.0000 (1.0)        245.3830 (1.0)       35.6531 (1.40)       234.8500 (1.0)       35.4000 (2.42)       194;53  4,075.2622 (1.0)        1146           1
test_word_count_rust_sequential                        1,131.9000 (6.00)     1,655.1000 (2.96)     1,188.3726 (4.84)      25.3759 (1.0)      1,185.4000 (5.05)      14.6000 (1.0)         73;45    841.4869 (0.21)        774           1
test_word_count_rust_sequential_twice_with_threads     1,211.0000 (6.42)     2,008.8000 (3.59)     1,464.1471 (5.97)     150.9719 (5.95)     1,445.5500 (6.16)     145.0000 (9.93)       119;49    682.9915 (0.17)        442           1
test_word_count_python_sequential                      7,180.6000 (38.07)    7,931.3000 (14.19)    7,529.9690 (30.69)    150.9821 (5.95)     7,512.9500 (31.99)    158.5500 (10.86)        27;7    132.8027 (0.03)        116           1
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Legend:
  Outliers: 1 Standard Deviation from Mean; 1.5 IQR (InterQuartile Range) from 1st Quartile and 3rd Quartile.
  OPS: Operations Per Second, computed as 1 / Mean
====================================================================================== 4 passed in 4.07s =======================================================================================
nox > Session bench was successful.
```

## 来源

Use [`cargo-generate`](https://crates.io/crates/cargo-generate):

```bash
$ cargo install cargo-generate
$ cargo generate --git https://github.com/PyO3/pyo3 examples/word-count
```

(`cargo generate` will take a little while to clone the PyO3 repo first; be patient when waiting for the command to run.)
